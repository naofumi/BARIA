package com.easwareapps.baria.fragments;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;


import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.easwareapps.baria.MainActivity;
import com.easwareapps.baria.utils.PInfo;
import com.easwareapps.baria.R;
import com.easwareapps.baria.adapter.InstalledAppsAdapter;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


public class InstalledAppsFragment extends Fragment {

    ArrayList<PInfo> res;
    RecyclerView apps;
    InstalledAppsAdapter installedAppsAdapter;
    static int iconSize = -1;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_installed_apps, container, false);
        apps = (RecyclerView) rootView.findViewById(R.id.appsList);

        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        apps.setLayoutManager(llm);
        getIconSize();
        new AppsLoader().execute();

        return rootView;
    }

    public void reloadApps() {
        apps.setAdapter(null);
        new AppsLoader().execute();
    }


    class AppsLoader extends AsyncTask <Void, Void, Void>{
        @Override
        protected Void doInBackground(Void... voids) {
            getApps();
            return null;
        }

        void getApps(){
            res = new ArrayList<>();
            PackageManager pm = getActivity().getPackageManager();

            List<PackageInfo> packs = pm.getInstalledPackages(0);
            for(PackageInfo p: packs) {
                Log.d("XYZ", p.packageName);
                try {
                    if(!MainActivity.showSystemApp) {
                        try {
                            ApplicationInfo ai = pm.getApplicationInfo(p.packageName, 0);
                            if ((ai.flags & ApplicationInfo.FLAG_SYSTEM) != 0) {
                                continue;
                            }
                        } catch (Exception e) {
                            Log.e("App Details", e.getLocalizedMessage());
                        }
                    }
                    PInfo newInfo = new PInfo();
                    newInfo.appname = p.applicationInfo.loadLabel(getActivity().getPackageManager()).toString();
                    newInfo.pname = p.packageName;
                    newInfo.versionName = p.versionName;
                    newInfo.versionCode = p.versionCode;
                    newInfo.apk = p.applicationInfo.sourceDir;
                    newInfo.icon = p.applicationInfo.loadIcon(getActivity().getPackageManager());


                    res.add(newInfo);
                } catch (Exception e) {

                    Log.d("Package Exception", e.getLocalizedMessage());
                }
            }
            Collections.sort(res, new Comparator<PInfo>() {

                public int compare(PInfo p1, PInfo p2) {
                    return p1.appname.compareToIgnoreCase(p2.appname);
                }
            });
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            installedAppsAdapter = InstalledAppsAdapter.getInstance(res,
                    getActivity(), getIconSize(), apps, getActivity());

            apps.setAdapter(installedAppsAdapter);
            installedAppsAdapter.requestUpdate();





        }
    }







    private int getIconSize(){
        if(iconSize == -1) {
            DisplayMetrics dm = new DisplayMetrics();
            getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
            final int width = (dm.widthPixels > dm.heightPixels) ? dm.heightPixels:dm.widthPixels;
            iconSize = width / 5;
        }
        return iconSize;
    }

    public void closeActionMode() {
        installedAppsAdapter.closeActionMode();
    }



    public void saveApps() {
        if(checkStoragePermission())  installedAppsAdapter.saveApps();
    }

    @TargetApi(23)
    private boolean checkStoragePermission() {

        String permissions[] = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE};
        if(isPermissionGranted(permissions[0])) {
            return true;
        }
        getActivity().requestPermissions(permissions, 11);
        return false;

    }

    @TargetApi(23)
    private boolean isPermissionGranted(String permission){
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }else {
            int hasStoragePermission = getActivity().checkSelfPermission(permission);
            if (hasStoragePermission != PackageManager.PERMISSION_GRANTED) {
                return false;

            }
        }
        return true;
    }




}
